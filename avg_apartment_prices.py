import requests
from datetime import datetime

oslo_coordinates = {
    'city_code': 'OSL',
    'lat': 59.91273,
    'lon': 10.74609
}

trondheim_coordinates = {
    'city_code': 'TRD',
    'lat': 63.4305,
    'lon': 10.39506
}

kristiansand_coordinates = {
    'city_code': 'KRS',
    'lat': 58.14671,
    'lon': 7.9956
}

bergen_coordinates = {
    'city_code': 'BRG',
    'lat': 60.39358,
    'lon': 5.32479
}

stavanger_coordinates = {
    'city_code': 'SVG',
    'lat': 58.97005,
    'lon': 5.73336
}

apartment_properties = {
    'min_price': 2000000,
    'max_price': 5000000,
    'distance_from_centre': 3000,
    'max_common_expenses': 3000,
    'min_area': 45,
    'max_area': 50
}

print("------------------------------------------------------------------------------------------------")

for property in apartment_properties:
    print(str(property) + ": " + str(apartment_properties[property]))

print("------------------------------------------------------------------------------------------------")


def get_avg_price(coordinates, apartment_properties):

    params = {
        'searchkey': 'SEARCH_ID_REALESTATE_HOMES',
        'sort': 'PRICE_ASC',
        'vertical': 'realestate',
        'property_type': 3,  # 3 = apartment
        'area_from': apartment_properties['min_area'],  # min apartment area
        'area_to': apartment_properties['max_area'],  # max apartment area
        'lat': coordinates['lat'],
        'lon': coordinates['lon'],
        'radius': apartment_properties['distance_from_centre'],  # distance from city centre
        'rent_to': apartment_properties['max_common_expenses'],  # fellesutgifter
        'price_collective_from': apartment_properties['min_price'],
        'price_collective_to': apartment_properties['max_price']
    }

    response = requests.get("https://www.finn.no/api/search-qf", params=params)

    apartments = response.json()['docs']

    apartments_found = 0
    total_price = 0
    total_area = 0

    for apartment in apartments:

        if ('price_total' in apartment):
            total_price = total_price + apartment['price_total']['amount']
        elif ('price_range_total' in apartment):
            total_price = total_price + \
                apartment['price_range_total']['amount_from']

        total_area = total_area + apartment['area_range']['size_from']
        apartments_found = apartments_found + 1

        # if (apartments_found == 10):
        # break

    avg_price = total_price / apartments_found
    avg_area = total_area / apartments_found

    output = "avg. apartment price in " + coordinates['city_code'] + " is " + str(
        int(avg_price)) + "kr " + "with avg. area of " + str(int(avg_area)) + "m2 based on " + str(apartments_found) + " apartments"

    print(output)

# Run on different cities
get_avg_price(oslo_coordinates, apartment_properties)
get_avg_price(trondheim_coordinates, apartment_properties)
get_avg_price(kristiansand_coordinates, apartment_properties)
get_avg_price(bergen_coordinates, apartment_properties)
get_avg_price(stavanger_coordinates, apartment_properties)
print("------------------------------------------------------------------------------------------------")